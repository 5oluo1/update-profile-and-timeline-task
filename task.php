<?php 

class UserController{

    protected $model;
    protected $helper;

    public function __construct()
    {
        $this->model=new User();
        $this->helper=new Helper();
    }
    function update(Request $request)
  {
      $rules=[
      'fname' => 'nullable',
      'lname' => 'nullable',
      'country' => 'nullable',
      'email' => 'nullable|email',
      'phone' => 'nullable',
      'birthday' => 'nullable'
    ];

      $errors=[
      'email.email' => 'this email is not valid',
    ];

      //to check validate of request data
      $data=validator()->make($request->all(), $rules, $errors);

      //action take when store failed
      if ($data->fails()) {
          return $this->helper->responseJson(0, $data->errors());
      }
      $record = auth()->user();
      $record->update($request->all());

      if ($record->save()) {
          return $this->helper->responseJson(1, 'updated done',$record);
      }
  }

  public function timeline(Request $request){

    $categories = Category::where(function ($query) use ($request) {
        if ($request->name) {
            $query->where('name', 'LIKE', '%' . $request->name . '%');
        }
    })->paginate();

    $posts = Post::whereHas('category',function ($query) use ($request) {
        if ($request->name) {
            $query->where('name', 'LIKE', '%' . $request->name . '%');
        }
    })
    ->with('user')
    ->paginate();
    return $this->helper->responseJson(1, 'entry done',
     ['categories' => $categories,
    'posts' => $posts]);

  }
}